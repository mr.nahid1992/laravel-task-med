<?php

namespace App\Http\Controllers;

use App\Services\TransectionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    private $transectionService;
    public function __construct(TransectionService $transectionService)
    {
        $this->transectionService = $transectionService;
    }
    public function index()
    {
        $data = array();
        $data['current_balance']=$this->transectionService->current_balance(Auth::id())?$this->transectionService->current_balance(Auth::id()):0;
        $data['all_transection'] = $this->transectionService->transection_list(Auth::id(), '');
        return view('home', $data);

    }
}
