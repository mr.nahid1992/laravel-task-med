<?php

use Illuminate\Support\Facades\Route;
use  App\Http\Controllers\HomeController;
use  App\Http\Controllers\TransectionController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;


Route::get('/', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('/login', [LoginController::class, 'login'])->name('login');
Route::post('/logout', [LoginController::class, 'logout'])->name('logout');


Route::get('/register', [RegisterController::class, 'showRegistrationForm'])->name('register');
Route::post('/register', [RegisterController::class, 'register'])->name('register');


Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', [HomeController::class, 'index'])->name('home');


    Route::get('/deposit', [TransectionController::class, 'deposit_list'])->name('deposit_list');
    Route::post('/deposit', [TransectionController::class, 'deposit_store'])->name('deposit_store');

    Route::get('/withdrawal', [TransectionController::class, 'withdrawal_list'])->name('withdrawal_list');
    Route::post('/withdrawal', [TransectionController::class, 'withdrawal_store'])->name('withdrawal_store');
});

